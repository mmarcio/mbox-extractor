help:
	@echo
	@echo "  Usage:"
	@echo "    make test    # build for testing"
	@echo "    make deploy  # build for deploy"
	@echo

test: MboxExtractor.icns
	python setup.py py2app -A

deploy: MboxExtractor.icns
	python setup.py py2app

MboxExtractor.icns: MboxExtractor.png
	python generate-iconset.py MboxExtractor.png

clean:
	rm -rf build
	rm MboxExtractor.icns
