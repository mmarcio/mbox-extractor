#!/usr/bin/env python

"""
Tool for the extraction of .eml files from one .mbox file

Author: Michele Marcionelli
Website: people.math.ethz.ch/~michele
Last modified: March 2021
"""

import os, sys, re, wx, mailbox, email.utils
from email.header import Header, decode_header, make_header
from datetime import datetime

class MboxExtractor(wx.Frame):

    def __init__(self, *args, **kw):

        super(MboxExtractor, self).__init__(*args, **kw)

        self.InitUI()
        self.Centre()
        self.SetSize((self.width, self.height))
        self.Show()

    def InitUI(self):

        self.width = 400
        self.height = 160
        self.margin = 15

        p = wx.Panel(self)
        vbox = wx.BoxSizer(wx.VERTICAL)

        # header
        font = wx.Font(14, wx.DEFAULT, wx.NORMAL, wx.BOLD)
        header = wx.StaticText(p, label = 'Select one .mbox file to extract', style = wx.ALIGN_CENTRE)
        header.SetFont(font)
        vbox.Add(header, 0, wx.ALL, self.margin)

        # progress bar
        self.gauge = wx.Gauge(p, range=1000, size=(self.width-2*self.margin, -1), style =  wx.GA_HORIZONTAL)
        vbox.Add(self.gauge, 0, wx.LEFT|wx.RIGHT|wx.ALIGN_CENTER_HORIZONTAL, self.margin)

        # BEGIN filename & select button horizontal group
        hbox = wx.BoxSizer(wx.HORIZONTAL)

        # filename
        self.FilenameCtrl = wx.StaticText(p, label='(no file selected)')
        hbox.Add(self.FilenameCtrl,0,wx.EXPAND)

        hbox.AddStretchSpacer(1)

        # select button
        self.btnSelect = wx.Button(p, label='Select')
        self.btnSelect.Bind(wx.EVT_BUTTON, self.OnSelect)
        hbox.Add(self.btnSelect, 0)

        vbox.Add(hbox, 0, wx.ALL|wx.EXPAND, self.margin)
        # END filename & select button horizontal group

        p.SetSizer(vbox)

    def OnSelect(self, e):

        frame = wx.Frame(self, -1)
        openFileDialog = wx.FileDialog(frame, "", "", "", "*.mbox", wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)
        openFileDialog.ShowModal()

        filename = openFileDialog.GetPath()
        openFileDialog.Destroy()

        if filename != '':
            destDir = filename[:-5] + '.dir'
            if os.path.isfile(filename):
                self.FilenameCtrl.SetLabel('Extracting ' + os.path.basename(filename) + '...')
                self.btnSelect.Disable()
                wx.Yield()
                self.ExtractEmails(filename, destDir)
                self.btnSelect.Enable()
                self.FilenameCtrl.SetLabel('Extraction finished!')
            else:
                if os.path.isfile(filename + os.path.sep + 'mbox'):
                    self.FilenameCtrl.SetLabel('Extracting ' + os.path.basename(filename) + '...')
                    self.btnSelect.Disable()
                    wx.Yield()
                    self.ExtractEmails(filename + os.path.sep + 'mbox', destDir)
                    self.btnSelect.Enable()
                    self.FilenameCtrl.SetLabel('Extraction finished!')
                else:
                    filenameLabel = 'No "mbox" file found in selected directory!'


    def createDateTime(self,timeString):

        def isTimeFormat(input, format):
            try:
                datetime.strptime(input, format)
                return True
            except ValueError:
                return False

        timeformat = '%a, %d %b %Y %H:%M:%S %z'
        timeformatGMT = '%a, %d %b %Y %H:%M:%S %z (%Z)'

        if isTimeFormat(timeString, timeformat):
            dt = datetime.strptime(timeString, timeformat)
            return dt

        elif isTimeFormat(timeString, timeformatGMT):
            dt = datetime.strptime(timeString, timeformatGMT)
            return dt

        else:
            print('problem with datetime: ' + timeString)
            raise

    def createSafeFilename(self,string):
        def safeChar(c):
            if c.isalnum() or c in ' ()[]{}!,.;@$%*«»<>&|-+=?_':
                return c
            else:
                return '_'
        return ''.join(safeChar(c) for c in string).strip('_').strip(' ')[0:60]


    def ExtractEmails(self, filename, destDir):

        try:
            os.mkdir(destDir)
        except OSError:
            pass

        # convert \r to \n, needed for Outlook for Mac exports
        mboxTmp = os.path.sep + 'tmp' + os.path.sep + 'mboxtmp.' + str(os.getpid())
        with open(filename, mode='rb') as fileIn:
            filedata = fileIn.read()
        filedata = filedata.replace(b'\r', b'\n')
        with open(mboxTmp, mode='wb') as fileOut:
            fileOut.write(filedata)

        count = 0
        messages = mailbox.mbox(mboxTmp)
        totMessages = messages.__len__()
        for message in messages:

            # date & time
            try:
                dt = self.createDateTime(message['date']).strftime('%Y-%m-%d %Hh%Mm%Ss')
            except:
                continue # if no date time => skip... buggy message?

            # subject
            try:
                s = str(make_header(decode_header(message['subject'])))
            except:
                s = '(empty)'

            # from
            match = re.search(r'[\w\.-]+@[\w\.-]+', message['from'])
            f = match.group(0)

            # destFile
            destFile = destDir + os.path.sep + dt + ' ' + self.createSafeFilename(s) + ' [' + f + '].eml'

            # save e-mail
            with open(destFile, 'w') as fileOut:
                print(destFile)
                print(message, file=fileOut)
            count+=1
            self.gauge.SetValue(int(count/totMessages*1000))
            wx.Yield()

        os.remove(mboxTmp)

def main():

    app = wx.App()
    MboxExtractor(None, title = 'Mbox Extractor')
    app.MainLoop()


if __name__ == '__main__':
    main()
