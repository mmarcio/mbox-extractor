# Mbox Extractor

Below we explain how to **export e-mails** with different e-mail clients. There is a difference if you export just one e-mail or a whole mailbox. If you export one e-mail you will be able to reopen it as expected with  just a double-click. On the other side, if you export a whole mailbox you will get 1 file containing all the e-mails and this file can not be opened just like expected (with double-click) anymore. To open it, you will have to **convert** it by extracting all e-mails into single files, using for instance [this software](#Software).

## Thunderbird

> Valid for all systems (Linux, MacOS & Windows)

### Export one e-mail

![thunderbird-export-1-email](images/thunderbird-export-1-email.jpg)

### Export a mailbox

To export a whole mailbox you will need to use a "plugin", that you first need to install.

#### Install then "ImportExportTools NG" plugin

![thunderbird-install-plugin-1](images/thunderbird-install-plugin-1.jpg)

![thunderbird-install-plugin-2](images/thunderbird-install-plugin-2.jpg)

![thunderbird-install-plugin-3](images/thunderbird-install-plugin-3.jpg)

#### How to use the plugin

![thunderbird-export-mailbox](images/thunderbird-export-mailbox.jpg)

## Mac Mail

> Valid vor MacOS

### Export one e-mail

![macmail-export-1-email](images/macmail-export-1-email.jpg)

### Export a mailbox

![macmail-export-mailbox](images/macmail-export-mailbox.jpg)

## Microsoft Outlook

> Valid for MacOS & Windows

### Export one e-mail

![outlook-export-1-email](images/outlook-export-1-email.jpg)

### Export a mailbox

![outlook-export-mailbox](images/outlook-export-mailbox.jpg)

## Software

### Download

| download                                                     | file size |
| ------------------------------------------------------------ | --------: |
| [Binary for macOS 10.15 (Intel)](https://people.math.ethz.ch/~mmarcio/MboxExtractor-macos-10.15-intel.zip) |     46 MB |
| [Binary for macOS 11 (Intel)](https://people.math.ethz.ch/~mmarcio/mbox-extractor/MboxExtractor-macos-11-intel.zip) |     44 MB |
| Binary for macOS 11 (Silicon) - coming soon                  |       n/a |
| [Python source](https://gitlab.math.ethz.ch/mmarcio/mbox-extractor/-/raw/main/mbox-extractor.py) |     ~6 KB |

### Usage

Start the program (double-click), open a `mbox` file and wait few seconds. The `mbox` file will be extracted into many single files at the same place of the `mbox` file.

For linux execute the "source" with python3: `python3 mbox-extractor.py`.

### Standalone macOS App

Install with [MacPorts](https://www.macports.org/) the dependencies `py39-py2app` and `py39-wxpython-4.0` and set `python39` as your default python:

```bash
sudo port install py39-py2app
sudo port install py39-wxpython-4.0
sudo port select --set python python39
```

To create the app just run

```bash
git clone https://gitlab.math.ethz.ch/mmarcio/mbox-extractor.git
cd mbox-extractor
make deploy
```



